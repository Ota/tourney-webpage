<?php
/**
 * @file
 * tourney_views.features.inc
 */

/**
 * Implements hook_views_api().
 */
function tourney_views_views_api() {
  return array("api" => "3.0");
}

/**
 * Implements hook_node_info().
 */
function tourney_views_node_info() {
  $items = array(
    'turnir' => array(
      'name' => t('Turnir'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  return $items;
}
