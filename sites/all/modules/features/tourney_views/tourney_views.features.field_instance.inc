<?php
/**
 * @file
 * tourney_views.features.field_instance.inc
 */

/**
 * Implements hook_field_default_field_instances().
 */
function tourney_views_field_default_field_instances() {
  $field_instances = array();

  // Exported field_instance: 'node-turnir-field_datum'
  $field_instances['node-turnir-field_datum'] = array(
    'bundle' => 'turnir',
    'deleted' => 0,
    'description' => 'Kdaj se bo turnir odvijal',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'date',
        'settings' => array(
          'format_type' => 'long',
          'fromto' => 'both',
          'multiple_from' => '',
          'multiple_number' => '',
          'multiple_to' => '',
        ),
        'type' => 'date_default',
        'weight' => 1,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'field_datum',
    'label' => 'Datum',
    'required' => 1,
    'settings' => array(
      'default_value' => 'now',
      'default_value2' => 'same',
      'default_value_code' => '',
      'default_value_code2' => '',
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'date',
      'settings' => array(
        'increment' => 15,
        'input_format' => 'm/d/Y - H:i:s',
        'input_format_custom' => '',
        'label_position' => 'above',
        'text_parts' => array(),
        'year_range' => '-3:+3',
      ),
      'type' => 'date_popup',
      'weight' => 1,
    ),
  );

  // Exported field_instance: 'node-turnir-field_igrisce'
  $field_instances['node-turnir-field_igrisce'] = array(
    'bundle' => 'turnir',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => 'Na zemljevidu, s klikom, izberite točno lokacijo igrišča.',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'location_cck',
        'settings' => array(),
        'type' => 'location_default',
        'weight' => 9,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'field_igrisce',
    'label' => 'Igrišče',
    'required' => 1,
    'settings' => array(
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 0,
      'module' => 'location_cck',
      'settings' => array(),
      'type' => 'location',
      'weight' => 8,
    ),
  );

  // Exported field_instance: 'node-turnir-field_kontaktne_osebe'
  $field_instances['node-turnir-field_kontaktne_osebe'] = array(
    'bundle' => 'turnir',
    'default_value' => array(
      0 => array(
        'value' => 'Janez Novak (040 000 111)',
      ),
    ),
    'deleted' => 0,
    'description' => 'Kontatkna oseba. Lahko jih dodate največ 4.',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'text',
        'settings' => array(),
        'type' => 'text_default',
        'weight' => 7,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'field_kontaktne_osebe',
    'label' => 'Kontaktne osebe',
    'required' => 1,
    'settings' => array(
      'text_processing' => 0,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'text',
      'settings' => array(
        'size' => 60,
      ),
      'type' => 'text_textfield',
      'weight' => 7,
    ),
  );

  // Exported field_instance: 'node-turnir-field_kraj_geocode'
  $field_instances['node-turnir-field_kraj_geocode'] = array(
    'bundle' => 'turnir',
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'geofield',
        'settings' => array(
          'data' => 'full',
        ),
        'type' => 'geofield_wkt',
        'weight' => 8,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'field_kraj_geocode',
    'label' => 'Kraj geocode',
    'required' => 0,
    'settings' => array(
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'geocoder',
      'settings' => array(
        'delta_handling' => 'default',
        'geocoder_field' => 'field_igrisce',
        'geocoder_handler' => 'google',
        'handler_settings' => array(
          'google' => array(
            'all_results' => 0,
            'geometry_type' => 'point',
            'reject_results' => array(
              'APPROXIMATE' => 'APPROXIMATE',
              'GEOMETRIC_CENTER' => 0,
              'RANGE_INTERPOLATED' => 0,
              'ROOFTOP' => 0,
            ),
          ),
        ),
      ),
      'type' => 'geocoder',
      'weight' => 9,
    ),
  );

  // Exported field_instance: 'node-turnir-field_nagrade'
  $field_instances['node-turnir-field_nagrade'] = array(
    'bundle' => 'turnir',
    'default_value' => array(
      0 => array(
        'value' => '1. mesto: 
2. mesto:
3. mesto:
4. mesto:',
      ),
    ),
    'deleted' => 0,
    'description' => 'Nagrade turnirja',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'text',
        'settings' => array(),
        'type' => 'text_default',
        'weight' => 4,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'field_nagrade',
    'label' => 'Nagrade',
    'required' => 0,
    'settings' => array(
      'text_processing' => 0,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'text',
      'settings' => array(
        'rows' => 5,
      ),
      'type' => 'text_textarea',
      'weight' => 4,
    ),
  );

  // Exported field_instance: 'node-turnir-field_opis_turnirja'
  $field_instances['node-turnir-field_opis_turnirja'] = array(
    'bundle' => 'turnir',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => 'Če želite še kaj dodati v zvezi s turnirjem',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'text',
        'settings' => array(),
        'type' => 'text_default',
        'weight' => 6,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'field_opis_turnirja',
    'label' => 'Opis turnirja',
    'required' => 0,
    'settings' => array(
      'text_processing' => 0,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'text',
      'settings' => array(
        'rows' => 5,
      ),
      'type' => 'text_textarea',
      'weight' => 6,
    ),
  );

  // Exported field_instance: 'node-turnir-field_panoga'
  $field_instances['node-turnir-field_panoga'] = array(
    'bundle' => 'turnir',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => 'V katero športno panogo spada turnir. Če med panogami ni vašega športa izberite "Drugi šport". Pišite nam če hočete, da se med športe doda nova panoga.',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'taxonomy',
        'settings' => array(),
        'type' => 'taxonomy_term_reference_plain',
        'weight' => 2,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'field_panoga',
    'label' => 'Panoga',
    'required' => 1,
    'settings' => array(
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'options',
      'settings' => array(),
      'type' => 'options_select',
      'weight' => 2,
    ),
  );

  // Exported field_instance: 'node-turnir-field_prijavnina'
  $field_instances['node-turnir-field_prijavnina'] = array(
    'bundle' => 'turnir',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => 'Prijavnina za turnir (npr: 50€).',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'text',
        'settings' => array(),
        'type' => 'text_default',
        'weight' => 5,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'field_prijavnina',
    'label' => 'Prijavnina',
    'required' => 1,
    'settings' => array(
      'text_processing' => 0,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'text',
      'settings' => array(
        'size' => 60,
      ),
      'type' => 'text_textfield',
      'weight' => 5,
    ),
  );

  // Translatables
  // Included for use with string extractors like potx.
  t('Datum');
  t('Igrišče');
  t('Kdaj se bo turnir odvijal');
  t('Kontaktne osebe');
  t('Kontatkna oseba. Lahko jih dodate največ 4.');
  t('Kraj geocode');
  t('Na zemljevidu, s klikom, izberite točno lokacijo igrišča.');
  t('Nagrade');
  t('Nagrade turnirja');
  t('Opis turnirja');
  t('Panoga');
  t('Prijavnina');
  t('Prijavnina za turnir (npr: 50€).');
  t('V katero športno panogo spada turnir. Če med panogami ni vašega športa izberite "Drugi šport". Pišite nam če hočete, da se med športe doda nova panoga.');
  t('Če želite še kaj dodati v zvezi s turnirjem');

  return $field_instances;
}
