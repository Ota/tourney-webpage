// JavaScript should be made compatible with libraries other than jQuery by
// wrapping it with an "anonymous closure". See:
// - http://drupal.org/node/1446420
// - http://www.adequatelygood.com/2010/3/JavaScript-Module-Pattern-In-Depth

(function ($, Drupal, window, document, undefined) {

  Drupal.behaviors.loadMap = {

    attach: function() {

      $('.attachment .view-zemljevid-turnirjev').jScrollPane(
        {
          verticalDragMinHeight: 80,
          verticalDragMaxHeight: 100,
          showArrows: true,
          verticalGutter: 20
        });

      myInfoWindow = new google.maps.InfoWindow;

      $('.map-address').click(function() {
        var coords = $(this).attr("coords").split(", ");

        panToPoint = new google.maps.LatLng(coords[0], coords[1]);
        var myinfoWindowContent = $(this).html();
        Drupal.gmap.getMap('gmap-auto1map-gmap0').map.panTo(panToPoint);

        myInfoWindow.open(Drupal.gmap.getMap('gmap-auto1map-gmap0').map);
        myInfoWindow.setPosition(panToPoint);
        myInfoWindow.setContent("<div class='myPopup'>" + myinfoWindowContent + "</div>");

      });
    }
  };

  Drupal.behaviors.addPlaceholder = {

    attach: function() {

      $('#edit-field-geofield-distance-origin').attr("placeholder", "kraja ...");

    }
  };

})(jQuery, Drupal, this, this.document);
